/* Set navigation */

function openNav() {
  $("#mySidenav").addClass("width80");
  $("#nav-res").addClass("opacityon");
  $(".cd-shadow-layer").addClass("displayblock");
  $(".wrapper").addClass("position-fixed");
}

function closeNav() {
  $("#mySidenav").removeClass("width80");
  $("#nav-res").removeClass("opacityon");
  $(".cd-shadow-layer").removeClass("displayblock");
  $(".wrapper").removeClass("position-fixed");
} 


$(document).ready(function(){ 

  $(".cd-shadow-layer").click(function(){
    closeNav(); 
  });
  
   // Add minus icon for collapse element which is open by default
   $(".collapse.show").each(function(){
    $(this).prev(".card-header").find(".flaticon-right-arrow").addClass("up-arrow-icon").removeClass("down-arrow-icon");
  });
  
  // Toggle plus minus icon on show hide of collapse element
  $(".collapse").on('show.bs.collapse', function(){
    $(this).prev(".card-header").find(".flaticon-right-arrow").removeClass("down-arrow-icon").addClass("up-arrow-icon");
  }).on('hide.bs.collapse', function(){
    $(this).prev(".card-header").find(".flaticon-right-arrow").removeClass("up-arrow-icon").addClass("down-arrow-icon");
  }); 

  Waves.attach('.btn', ['waves-effect']);

 });

/* end of navigation */



/* search box  */


function openSearch() {
  document.getElementById("search-boxes").style.height = "100%";
}

function closeSearch() {
  document.getElementById("search-boxes").style.height = "0%";
}

function DropDown(el) {
  this.dd = el;
  this.placeholder = this.dd.children('span');
  this.opts = this.dd.find('ul.dropdown > li');
  this.val = '';
  this.index = -1;
  this.initEvents();
}
DropDown.prototype = {
  initEvents : function() {
    var obj = this;

    obj.dd.on('click', function(event){
      $(this).toggleClass('active');
      return false;
    });

    obj.opts.on('click',function(){
      var opt = $(this);
      obj.val = opt.text();
      obj.index = opt.index();
      obj.placeholder.text(obj.val);
    });
  },
  getValue : function() {
    return this.val;
  },
  getIndex : function() {
    return this.index;
  }
}

$(function() {

  var dd = new DropDown( $('#dd') );

  $(document).click(function() {
    // all dropdowns
    $('.wrapper-dropdown-3').removeClass('active');
  });

});


/* Naviagation Effect */

  'use strict';
  
  var c, currentScrollTop = 0,
   navbar = $('.header-div');
 
   
     $(window).scroll(function () {
     var a = $(window).scrollTop();
     var b = navbar.height();
    
     currentScrollTop = a;
    
     if (c < currentScrollTop && a > b + b) {
       navbar.addClass("scrollUp");
       navbar.removeClass("header-bgcolor");
     } else if (c > currentScrollTop && !(a <= b)) {
       navbar.removeClass("scrollUp");
       navbar.addClass("header-bgcolor");
     }
     c = currentScrollTop;

     if (a <= 50) navbar.removeClass('header-bgcolor');
     
  });
  




  
  $(document).ready(function () {

    $('#testimonial-owl').owlCarousel({
      loop:true,
      nav:true,
      navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/left.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/right.svg" class="right_arrow_icon" alt="arrow" /></span>'],
      dots: true,
      stagePadding: 0,
      // animateIn: 'fadeIn',
      // animateOut: 'fadeOut',
      smartSpeed:2000,
      responsive:{
          0:{
              items:1 
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
  });

  $('#owl-slider2').owlCarousel({
    loop:false,
    nav:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/left.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/right.svg" class="right_arrow_icon" alt="arrow" /></span>'],
    dots: false,
    stagePadding: 0,
    smartSpeed:2000,
    responsive:{
        0:{
            items:1 
        },
        600:{
            items:2
        },
        1000:{
            items:2
        },
        1180:{
          items:3
        } 

    }
  });

  $('#gallery-owl').owlCarousel({
    loop:true,
    nav:false,
    center:true,
    // dotsEach: true,
    autoplay: true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/left.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/right.svg" class="right_arrow_icon" alt="arrow" /></span>'],
    dots: true,
    stagePadding: 0,
    autoplayHoverPause:true,
    smartSpeed:2000,
    responsiveClass: true,
    responsive:{
        0:{
            items:1 
        },
        600:{
            items:2
        },
        1000:{
            items:4
        },
        1180:{
          items:5
        } 

    }
  });

  $('#career-owl').owlCarousel({
    loop:true,
    nav:false,
    
    // dotsEach: true,
    autoplay: true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icons/left.svg" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icons/right.svg" class="right_arrow_icon" alt="arrow" /></span>'],
    dots: false,
    stagePadding: 0,
    autoplayHoverPause:true,
    smartSpeed:2000,
    responsiveClass: true,
    responsive:{
        0:{
            items:1 
        },
        600:{
            items:2
        },
        1000:{
            items:3
        },
        1180:{
          items:4
        } 

    }
  });

  

 


});
  
  /*
$(window).scroll(function(){
  var sticky = $('.header-div'),
      scroll = $(window).scrollTop();

    if (scroll >= 100) sticky.addClass('fixed_top');
    else sticky.removeClass('fixed_top');

  }); */


  
$(window).scroll(function() {
  if ($(this).scrollTop() >= 150) {        
      $('.return-to-top').addClass("display_show");    
  } else {
      $('.return-to-top').removeClass("display_show");   
  }
});

$('.return-to-top').click(function() {    
  $('body,html').animate({
       scrollTop : 0                       
   }, 1000);
 });